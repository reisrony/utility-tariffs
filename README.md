# Utility Tariffs

Program which matches utility tariffs to customers based on usage.

----

## Getting Started

These instructions will get you a copy of the project up and running on your local machine. 

### Prerequisites

[Java Runtime Environment (JRE)](https://www.oracle.com/technetwork/java/javase/downloads/index.html)

### Running

A step by step series of examples that tell you how to get running

Say what the step will be

```
Give the example
```


## Built With

* [Maven](https://maven.apache.org/) - Dependency Management


## Author

* **Roni Reis**
