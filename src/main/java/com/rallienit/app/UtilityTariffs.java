/*
 * Copyright (c) 2021 Rallien IT - All Rights Reserved.
 */

package com.rallienit.app;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Matches tariffs to customers based on usage.
 *
 * @author Roni Reis
 */
public interface UtilityTariffs {
    
    /**
     * For the given annual kWh consumption(s), output an annual cost (inclusive of VAT) for applicable tariffs,
     * sorted by cheapest first.
     * Each tariff should be printed on its own line in the format TARIFF_NAME COST.
     * If any usage is '0' assume the customer is not being supplied for that fuel type.
     *
     * @param powerConsumption - Annual power consumption
     * @param gasConsumption - Annual gas consumption
     *
     * @return Annual cost (inclusive of VAT) for applicable tariffs, sorted by cheapest first.
     */
    Map<String, BigDecimal> tariffsAnnualCost(final int powerConsumption, final int gasConsumption);
    
    /**
     * For the specified tariff calculate how much energy (in kWh) would be used annually
     * from a monthly spend in pounds (inclusive of VAT). Fuel type can be power or gas.
     *
     * @param tariffName - Tariff name
     * @param fuelType - Fuel type (power or gas)
     * @param targetMonthlySpend - Target monthly spend in pounds
     *
     * @return The total annual consumption.
     */
    int tariffAnnualUsage(final String tariffName, final String fuelType, final double targetMonthlySpend);
}
