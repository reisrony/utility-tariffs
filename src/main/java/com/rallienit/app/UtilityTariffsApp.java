/*
 * Copyright (c) 2021 Rallien IT - All Rights Reserved.
 */

package com.rallienit.app;

import java.math.BigDecimal;
import java.util.Map;

public class UtilityTariffsApp {
    
    public static void main(String... args) {
        parseCmdLnArgs(args);
    }
    
    private static void parseCmdLnArgs(String... args) {
        final String defaultFile = "prices.json";
        
        if (args.length == 0) {
            printArgsError();
        } else {
            String cmd = args[0];
            
            switch (cmd) {
                case "cost", "COST" -> {
                    if ((args.length - 1) > 1) {
                        final int power = parseConsumption(args[1]);
                        final int gas = parseConsumption(args[2]);
                        final String file = args.length == 4 ? args[3] : defaultFile;
                        final UtilityTariffs utilityTariffs = new UtilityTariffsImp(file);
                        final Map<String, BigDecimal> tariffs = utilityTariffs.tariffsAnnualCost(power, gas);
                        
                        tariffs.forEach((trf, cost) -> System.out.println(trf + " £" + cost));
                    } else {
                        printArgsError();
                    }
                }
                case "usage", "USAGE" -> {
                    if ((args.length - 1) > 2) {
                        final String tariff = args[1];
                        final String fuel = args[2];
                        final double target = parseMonthlyExpenditure(args[3]);
                        
                        checkIfIsCorrectFuelType(fuel);
                        
                        final String file = args.length == 5 ? args[4] : defaultFile;
                        final UtilityTariffs utilityTariffs = new UtilityTariffsImp(file);
                        final int usage = utilityTariffs.tariffAnnualUsage(tariff, fuel, target);
                        System.out.println(usage + "kWh");
                    } else {
                        printArgsError();
                    }
                }
                case "help", "HELP" -> printArgsHelp();
                default -> printArgsError();
            }
        }
    }
    
    private static void printArgsError() {
        String error = """
                USAGE:
                   
                   cost <POWER_USAGE> <GAS_USAGE> [FILE]
                   
                where
                   POWER_USAGE     Annual power consumption (if fuel type not supplied use '0')
                   GAS_USAGE       Annual gas consumption (if fuel type not supplied use '0')
                   
                options
                   FILE    JSON file with tariffs data (default: prices.json)
                   
                   
                   usage <TARIFF_NAME> <FUEL_TYPE> <TARGET_MONTHLY_SPEND> [FILE]
                   
                where
                   TARIFF_NAME             Tariff name
                   FUEL_TYPE               Fuel type (power or gas)
                   TARGET_MONTHLY_SPEND    Target monthly spend in pounds
                   
                options
                   FILE    JSON file with tariffs data (default: prices.json)
                """;
        System.out.println(error);
        System.exit(1);
    }
    
    private static void printArgsHelp() {
        String help = """
                Matches tariffs to customers:
                - Represent tariffs according to how much energy is annually consumed.
                - Calculate annual energy usage based on a target monthly expenditure.
                      
                      USAGE:
                       
                      	cost <POWER_USAGE> <GAS_USAGE> [FILE]
                       
                      where
                      	POWER_USAGE     Annual power consumption (if fuel type not supplied use '0')
                      	GAS_USAGE       Annual gas consumption (if fuel type not supplied use '0')
                       
                      options
                        FILE    JSON file with tariffs data (default: prices.json)
                        
                        
                      	usage <TARIFF_NAME> <FUEL_TYPE> <TARGET_MONTHLY_SPEND> [FILE]
                       
                      where
                      	TARIFF_NAME             Tariff name
                      	FUEL_TYPE               Fuel type (power or gas)
                      	TARGET_MONTHLY_SPEND    Target monthly spend in pounds
                       
                      options
                        FILE    JSON file with tariffs data (default: prices.json)
                """;
        System.out.println(help);
        System.exit(0);
    }
    
    private static void checkIfIsCorrectFuelType(String fuelType) {
        if (!("power".equalsIgnoreCase(fuelType) || "gas".equalsIgnoreCase(fuelType))) {
            System.out.println("Fuel type must be power or gas.");
            System.exit(1);
        }
    }
    
    private static int parseConsumption(String cons) {
        int result = 0;
        
        try {
            result = Integer.parseInt(cons);
        } catch (NumberFormatException ex) {
            System.out.println("Annual consumption must be a whole number.");
            System.out.println(ex.getLocalizedMessage());
            System.exit(1);
        }
        
        return result;
    }
    
    private static double parseMonthlyExpenditure(String exp) {
        double result = 0;
        
        try {
            result = Double.parseDouble(exp);
        } catch (NumberFormatException ex) {
            System.out.println("Target monthly expenditure must be a number.");
            System.out.println(ex.getLocalizedMessage());
            System.exit(1);
        }
        
        return result;
    }
}
