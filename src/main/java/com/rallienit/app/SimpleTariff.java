/*
 * Copyright (c) 2021 Rallien IT - All Rights Reserved.
 */

package com.rallienit.app;

import java.math.BigDecimal;

public final class SimpleTariff implements Tariff {
    
    private final String name;
    private final BigDecimal gas;
    private final BigDecimal power;
    private final BigDecimal charge;
    
    public SimpleTariff(final String name, final double gasCost, final double powerCost, final double monthlyCharge) {
        this.name = name;
        this.gas = BigDecimal.valueOf(gasCost);
        this.power = BigDecimal.valueOf(powerCost);
        this.charge = BigDecimal.valueOf(monthlyCharge);
    }
    
    @Override
    public boolean hasGas() {
        return this.gas != null;
    }
    
    @Override
    public boolean hasPower() {
        return this.power != null;
    }
    
    @Override
    public String print() {
        return this.name;
    }
    
    @Override
    public BigDecimal power() {
        return this.power;
    }
    
    @Override
    public BigDecimal gas() {
        return this.gas;
    }
    
    @Override
    public BigDecimal charge() {
        return this.charge;
    }
}
