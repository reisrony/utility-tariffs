/*
 * Copyright (c) 2021 Rallien IT - All Rights Reserved.
 */

package com.rallienit.app;

import java.math.BigDecimal;

/**
 * Represent a tariff.
 *
 * @author Roni Reis
 */
public interface Tariff {
    
    /**
     * Check if the tariff includes gas.
     *
     * @return If has gas.
     */
    boolean hasGas();
    
    /**
     * Check if the tariff includes power.
     *
     * @return If has power.
     */
    boolean hasPower();
    
    /**
     * Print the tariff name.
     *
     * @return The tariff name.
     */
    String print();
    
    BigDecimal power();
    
    BigDecimal gas();
    
    BigDecimal charge();
}
