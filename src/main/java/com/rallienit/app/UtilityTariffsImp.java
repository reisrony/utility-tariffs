/*
 * Copyright (c) 2021 Rallien IT - All Rights Reserved.
 */

package com.rallienit.app;

import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

public final class UtilityTariffsImp implements UtilityTariffs {
    
    private static final int VAT = 5;
    private final String filePath;
    
    public UtilityTariffsImp(String filePath) {
        this.filePath = filePath;
    }
    
    @Override
    public Map<String, BigDecimal> tariffsAnnualCost(final int powerConsumption, final int gasConsumption) {
        Tariff[] tariffsList;
        
        try {
            tariffsList = this.getTariffsData();
        } catch (FileNotFoundException e) {
            System.err.println("File with tariffs data not found: " + this.filePath);
            return Collections.emptyMap();
        }
        
        final Map<String, BigDecimal> tariffsAnnualCost = new HashMap<>(tariffsList.length);
        
        for (Tariff tariff : tariffsList) {
            //
            // Annual Cost = (powerConsumption * powerCost + monthlyStandingCharge * 12) + (gasConsumption * gasCost + monthlyStandingCharge * 12) + VAT
            //
            BigDecimal annualPowerCost = BigDecimal.ZERO;
            BigDecimal annualGasCost = BigDecimal.ZERO;
            
            if (powerConsumption > 0 && gasConsumption > 0) {
                if (tariff.hasPower() && tariff.hasGas()) {
                    annualPowerCost = new BigDecimal(powerConsumption).multiply(tariff.power())
                            .add(tariff.charge().multiply(new BigDecimal(12)));
                    
                    annualGasCost = new BigDecimal(gasConsumption).multiply(tariff.gas())
                            .add(tariff.charge().multiply(new BigDecimal(12)));
                }
            } else if (powerConsumption > 0 && tariff.hasPower()) {
                annualPowerCost = new BigDecimal(powerConsumption).multiply(tariff.power())
                        .add(tariff.charge().multiply(new BigDecimal(12)));
            } else if (gasConsumption > 0 && tariff.hasGas()) {
                annualGasCost = new BigDecimal(gasConsumption).multiply(tariff.gas())
                        .add(tariff.charge().multiply(new BigDecimal(12)));
            }
            
            final BigDecimal annualCost = annualPowerCost.add(annualGasCost);
            final BigDecimal annualCostWithVat = annualCost.add(annualCost.multiply(
                    new BigDecimal(VAT).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP)));
            final BigDecimal result = annualCostWithVat.setScale(2, RoundingMode.HALF_UP);
            
            if (result.intValue() > 0) {
                tariffsAnnualCost.put(tariff.print(), result);
            }
        }
        
        return sortedByCheapest(tariffsAnnualCost);
    }
    
    @Override
    public int tariffAnnualUsage(final String tariffName, final String fuelType, final double targetMonthlySpend) {
        Tariff[] tariffsList;
        int annualUsage = 0;
        
        try {
            tariffsList = this.getTariffsData();
        } catch (FileNotFoundException e) {
            System.err.println("File with tariffs data not found: " + this.filePath);
            return annualUsage;
        }
        
        if (Arrays.stream(tariffsList).anyMatch(trf -> trf.print().equals(tariffName))) {
            final Optional<Tariff> tariff = Arrays.stream(tariffsList).filter(trf -> trf.print().equals(tariffName)).findFirst();
            //
            // Annual Usage = ((targetMonthlySpend - VAT - monthlyStandingCharge) / gasCost) * 12
            //
            final BigDecimal vatCost = BigDecimal.valueOf(targetMonthlySpend).multiply(
                    new BigDecimal(VAT).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP));

            if ("gas".equalsIgnoreCase(fuelType) && tariff.isPresent()) {
                annualUsage = BigDecimal.valueOf(targetMonthlySpend).subtract(vatCost).subtract(tariff.get().charge())
                        .divide(tariff.get().gas(), 2, RoundingMode.HALF_UP).multiply(new BigDecimal(12)).intValue();
            } else if ("power".equalsIgnoreCase(fuelType) && tariff.isPresent()) {
                annualUsage = BigDecimal.valueOf(targetMonthlySpend).subtract(vatCost).subtract(tariff.get().charge())
                        .divide(tariff.get().power(), 2, RoundingMode.HALF_UP).multiply(new BigDecimal(12)).intValue();
            }
        }
        
        return annualUsage;
    }
    
    private Tariff[] getTariffsData() throws FileNotFoundException {
        final Gson gson = new Gson();
        return gson.fromJson(new FileReader(this.filePath), SimpleTariff[].class);
    }
    
    private Map<String, BigDecimal> sortedByCheapest(final Map<String, BigDecimal> map) {
        if (map.isEmpty()) {
            return map;
        }
        
        return map.entrySet().stream().sorted(Map.Entry.comparingByValue()).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }
}
