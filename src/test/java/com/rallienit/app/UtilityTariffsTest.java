/*
 * Copyright (c) 2021 Rallien IT - All Rights Reserved.
 */

package com.rallienit.app;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UtilityTariffsTest {
    
    /**
     * tariffsAnnualCost can calculate power and gas usage annual cost
     */
    @Test
    void calculatePowerAndGasUsageAnnualCost() {
        final int powerUsage = 100;
        final int gasUsage = 100;
        
        final Map<String, BigDecimal> result = getTestUtilityTariffs().tariffsAnnualCost(powerUsage, gasUsage);
        
        assertEquals(3, result.size());
        assertEquals("1638.00", result.get("Tariff-1").toString());
    }
    
    /**
     * tariffsAnnualCost can calculate only power usage annual cost
     */
    @Test
    void calculateOnlyPowerUsageAnnualCost() {
        final int powerUsage = 100;
        final int gasUsage = 0;
        
        final Map<String, BigDecimal> result = getTestUtilityTariffs().tariffsAnnualCost(powerUsage, gasUsage);
        
        assertEquals(4, result.size());
        assertEquals("1029.00", result.get("Tariff-1").toString());
    }
    
    /**
     * tariffsAnnualCost can calculate only gas usage annual cost
     */
    @Test
    void calculateOnlyGasUsageAnnualCost() {
        final int powerUsage = 0;
        final int gasUsage = 100;
        
        final Map<String, BigDecimal> result = getTestUtilityTariffs().tariffsAnnualCost(powerUsage, gasUsage);
        
        assertEquals(4, result.size());
        assertEquals("609.00", result.get("Tariff-1").toString());
    }
    
    /**
     * tariffsAnnualCost cannot calculate annual cost with '0' fuel usage
     */
    @Test
    void annualCostWithZeroFuelUsage() {
        final int powerUsage = 0;
        final int gasUsage = 0;
        
        final Map<String, BigDecimal> result = getTestUtilityTariffs().tariffsAnnualCost(powerUsage, gasUsage);
        
        assertEquals(0, result.size());
    }
    
    /**
     * tariffAnnualUsage can calculate gas annual usage for specific tariff name
     */
    @Test
    void calculateGasAnnualUsageForSpecificTariff() {
        final String tariffName = "Tariff-2";
        final String fuelType = "gas";
        final double targetMonthlySpend = 100.50;
        
        final int result = getTestUtilityTariffs().tariffAnnualUsage(tariffName, fuelType, targetMonthlySpend);
        
        assertEquals(181, result);
    }
    
    /**
     * tariffAnnualUsage can calculate power annual usage for specific tariff name
     */
    @Test
    void calculatePowerAnnualUsageForSpecificTariff() {
        final String tariffName = "Tariff-2";
        final String fuelType = "power";
        final double targetMonthlySpend = 200.00;
        
        final int result = getTestUtilityTariffs().tariffAnnualUsage(tariffName, fuelType, targetMonthlySpend);
        
        assertEquals(204, result);
    }
    
    /**
     * tariffAnnualUsage cannot calculate power annual usage with wrong tariff name
     */
    @Test
    void calculatePowerAnnualUsageForWrongTariff() {
        final String tariffName = "Tariff-6";
        final String fuelType = "power";
        double targetMonthlySpend = 200.00;
        
        final int result = getTestUtilityTariffs().tariffAnnualUsage(tariffName, fuelType, targetMonthlySpend);
        
        assertEquals(0, result);
    }
    
    private UtilityTariffs getTestUtilityTariffs() {
        return new UtilityTariffsImp("src/main/resources/prices.json");
    }
}